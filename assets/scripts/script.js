// $(function() {
//   $('a[href*=#]').on('click', function(e) {
//     e.preventDefault();
//     $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
//   });
// });

const cannedItems = []
const saucesItems = []
const pastasAndRiceItems = []
const bakingItems = []
const snacksItems = []
const beveragesItems = []
const importedItems =[]
const dairyItems = []
const freshItems = []
const frozenItems = []
const othersItems = []


const addToListBtn = document.getElementById("addToListBtn")

const newItem = document.getElementById("addItemSection").firstElementChild.nextElementSibling

const selectCategory = document.getElementById("selectCategory")


addToListBtn.addEventListener("click", function(){
	// Clears all lists to make sure there aren't duplicates.
	document.getElementById("cannedList").innerHTML = "";
	document.getElementById("saucesList").innerHTML = "";
	document.getElementById("pastasAndRiceList").innerHTML = "";
	document.getElementById("bakingNeedsList").innerHTML = "";
	document.getElementById("snacksList").innerHTML = "";
	document.getElementById("beveragesList").innerHTML = "";
	document.getElementById("importedList").innerHTML = "";
	document.getElementById("dairyList").innerHTML = "";
	document.getElementById("freshProduceList").innerHTML = "";
	document.getElementById("frozenGoodsList").innerHTML = "";
	document.getElementById("othersList").innerHTML = "";

	// console.log(newItem.value)
	// console.log(selectCategory.value)

	// Adding Items to the Canned Goods List.
	if(newItem.value !== "" && selectCategory.value === "Canned Goods"){
		cannedItems.push(newItem.value)
	}
	cannedItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("cannedList").appendChild(newRow);
	})

		// Adding Items to the Condiments / Sauces List.
	if(newItem.value !== "" && selectCategory.value === "Condiments / Sauces"){
		saucesItems.push(newItem.value)
	}
	saucesItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("saucesList").appendChild(newRow);
	})

		// Adding Items to the Pastas and Rice List.
	if(newItem.value !== "" && selectCategory.value === "Pastas and Rice"){
		pastasAndRiceItems.push(newItem.value)
	}
	pastasAndRiceItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("pastasAndRiceList").appendChild(newRow);
	})

		// Adding Items to the Baking Needs List.
	if(newItem.value !== "" && selectCategory.value === "Baking Needs"){
		bakingItems.push(newItem.value)
	}
	bakingItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("bakingNeedsList").appendChild(newRow);
	})

		// Adding Items to the Snacks List.
	if(newItem.value !== "" && selectCategory.value === "Snacks"){
		snacksItems.push(newItem.value)
	}
	snacksItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("snacksList").appendChild(newRow);
	})

		// Adding Items to the Beverages List.
	if(newItem.value !== "" && selectCategory.value === "Beverages"){
		beveragesItems.push(newItem.value)
	}
	beveragesItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("beveragesList").appendChild(newRow);
	})

		// Adding Items to the Imported Items List.
	if(newItem.value !== "" && selectCategory.value === "Imported Items"){
		importedItems.push(newItem.value)
	}
	importedItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("importedList").appendChild(newRow);
	})

		// Adding Items to the Dairy and Cheeses List.
	if(newItem.value !== "" && selectCategory.value === "Dairy and Cheeses"){
		dairyItems.push(newItem.value)
	}
	dairyItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("dairyList").appendChild(newRow);
	})

		// Adding Items to the Fresh Produce List.
	if(newItem.value !== "" && selectCategory.value === "Fresh Produce"){
		freshItems.push(newItem.value)
	}
	freshItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("freshProduceList").appendChild(newRow);
	})

		// Adding Items to the Frozen Goods List.
	if(newItem.value !== "" && selectCategory.value === "Frozen Goods"){
		frozenItems.push(newItem.value)
	}
	frozenItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("frozenGoodsList").appendChild(newRow);
	})

		// Adding Items to the Others List.
	if(newItem.value !== "" && selectCategory.value === "Others"){
		othersItems.push(newItem.value)
	}
	othersItems.forEach(function(indivItem){
		const newRow = document.createElement('tr');

	newRow.innerHTML = `<td><button class="btn btn-success doneBtn">Bought</button></td>
						<td><button class="btn btn-secondary undoBtn">Undo</button></td>
						<td>${indivItem}</td>`

	document.getElementById("othersList").appendChild(newRow);
	})
})

document.addEventListener("click", function(event){
	if(event.target.classList.contains("doneBtn")){
		event.target.parentElement.parentElement.classList.add("bg-dark")
	}

	if(event.target.classList.contains("undoBtn")){
		event.target.parentElement.parentElement.classList.remove("bg-dark")
	}

})
